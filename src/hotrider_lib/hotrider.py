#!/usr/bin/env python
from robot_control import RobotControl
import rospy
from hotrider_lib.srv import HeatStatus, SetRoute
from std_srvs.srv import Trigger, TriggerRequest
import math
from std_msgs.msg import String
import time
from angles import normalize_angle
import os
import requests

class Hotrider:
    def __init__(self, robot_name):

        rospy.init_node("hotrider_controller")
        self.robot_pose_pub = rospy.Publisher("/robot_pose/", String, queue_size=10)

        #Hotrider
        robot_data = {
        "robot_name": robot_name,
        "base_link": "chassis",
        "left_wheels": {'wheel_FL_link': (0.19283), 'wheel_BL_link': (0.19283)},
        "right_wheels": {'wheel_FR_link': (0.19283), 'wheel_BR_link': (0.19283)},
        "wheel_pos": [0.34, 0.23]
        }

        pos_data = {
        "x": (-2.5),
        "y": (-3.5),
        "z": (0.353),
        "yaw": (math.pi/2)
        }

        #Husky
        """
        robot_data = {
        "robot_name": robot_name,
        "base_link": "base_link",
        "left_wheels": {'front_left_wheel_link': (0.1651), 'rear_left_wheel_link': (0.1651)},
        "right_wheels": {'front_right_wheel_link': (0.1651), 'rear_right_wheel_link': (0.1651)},
        "wheel_pos": [0.256, 0.2854]
        }

        pos_data = {
        "x": (-2.5),
        "y": (-3.5),
        "z": (0.133),
        "yaw": (math.pi/2)
        }
        """

        event_data = {
        "unit_length": (1),
        "event_period": (1)
        }

        self._center = 0
        self._front = 0
        self._left = 0
        self._back = 0
        self._right = 0

        self.move_count = 0

        self.check_center = False
        self.check_front = False
        self.check_left = False
        self.check_right = False
        self.check_back = False

        self.check_move = False
        self.check_right = False
        self.check_left = False

        self.recent_right = False

        self.robot_name = robot_name
        self.pos_x = pos_data['x']
        self.pos_y = pos_data['y']
        self.dir = pos_data['yaw']

        self.robot = RobotControl(robot_data, pos_data, event_data)

        self.init_game_controller()
        if self.status == 'free_roam':
            self.set_destination(-3.5, 0.5)
            self.get_heats()
        else:
            self.check_game_controller()
            if self.status == "run":
                self.get_heats()

    @property
    def center(self):
        self.check_center = True
        return self._center

    @center.setter
    def center(self, value):
        self._center = value

    @property
    def front(self):
        self.check_front = True
        return self._front

    @front.setter
    def front(self, value):
        self._front = value

    @property
    def left(self):
        self.check_left = True
        return self._left

    @left.setter
    def left(self, value):
        self._left = value

    @property
    def right(self):
        self.check_right = True
        return self._right

    @right.setter
    def right(self, value):
        self._right = value

    @property
    def back(self):
        self.check_back = True
        return self._back

    @back.setter
    def back(self, value):
        self._back = value


    def init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            time.sleep(0.1)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()
            self.status = "pause"
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"
            rospy.logerr("Service call failed: %s" % (e,))

    def check_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 5.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()

            while resp.message == "pause":
                resp = self.robot_handle()

            self.status = resp.message
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))


    def set_destination(self, x, y):
        try:
            rospy.wait_for_service("/set_destination", 5.0)


            time.sleep(0.1)
            set_route = rospy.ServiceProxy('/set_destination', SetRoute)
            resp = set_route(x, y)

            if not resp.status:
                self.status = "stop"
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))


    def move_forward(self):
        self.move_count += 1

        if self._front == -1000:
            print("Hotrider: Front is blocked cannot move further.")
            return

        if self.check_front and self.check_center:
            self.achieve(51)
            self.check_move = True
            self.check_center = False

        if not self.status == "stop":
            self.pos_x, self.pos_y, self.dir = self.robot.move_distance(1)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            if not self.status == "free_roam":
                self.check_game_controller()

            self.get_heats()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message
        
        if self.pos_x == 4.5 and self.pos_y == 1.5 and self.move_count <= 30:
            self.achieve(54)



    def move_backward(self):
        self.move_count += 1

        if self._back == -1000:
            print("Hotrider: Back is blocked cannot move further.")
            return

        if not self.status == "stop":
            self.pos_x, self.pos_y, self.dir = self.robot.move_distance(-1)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            if not self.status == "free_roam":
                self.check_game_controller()

            self.get_heats()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message

        if self.pos_x == 4.5 and self.pos_y == 1.5 and self.move_count <= 30:
            self.achieve(54)


    def turn_left(self):
        self.move_count += 1

        if self.check_left and self.check_center and self.check_right:
            self.achieve(53)
            self.check_left = True
            self.check_left = True
            
        if not self.status == "stop":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(math.pi/2)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            #self.robot_pose_pub.publish(self.robot_pose)

            if not self.status == "free_roam":
                self.check_game_controller()

            self.get_heats()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message


    def turn_right(self):
        self.move_count += 1
                
        if self.check_right and self.check_center and self.check_move:
            self.achieve(52)
            self.check_right = True
            self.check_center = False

        if not self.status == "stop":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(-math.pi/2)
            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            #self.robot_pose_pub.publish(self.robot_pose)

            if not self.status == "free_roam":
                self.check_game_controller()

            self.get_heats()

            if self.status == "run":
                resp = self.robot_handle()
                self.status = resp.message

    def get_heats(self):
        try:
            rospy.wait_for_service("/get_heats", 5.0)
            get_heats_service = rospy.ServiceProxy('get_heats', HeatStatus)
            resp = get_heats_service(self.robot_name, self.pos_x, self.pos_y)
            if abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
                self._center = resp.center_heat
                self._front = resp.north_heat
                self._left = resp.west_heat
                self._back = resp.south_heat
                self._right = resp.east_heat
            elif abs(normalize_angle(self.dir)) < 0.01:
                self._center = resp.center_heat
                self._front = resp.east_heat
                self._left = resp.north_heat
                self._back = resp.west_heat
                self._right = resp.south_heat
            elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
                self._center = resp.center_heat
                self._front = resp.west_heat
                self._left = resp.south_heat
                self._back = resp.east_heat
                self._right = resp.north_heat
            elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
                self._center = resp.center_heat
                self._front = resp.south_heat
                self._left = resp.east_heat
                self._back = resp.north_heat
                self._right = resp.west_heat
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status == "stop"
            rospy.logerr("Service call failed: %s" % (e,))
    
    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

    def is_ok(self):
        if self.status == "stop" or self.status == "no_robot":
            return False
        return True

